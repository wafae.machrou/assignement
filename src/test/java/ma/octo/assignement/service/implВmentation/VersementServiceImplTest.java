package ma.octo.assignement.service.implВmentation;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.entities.Compte;

import ma.octo.assignement.entities.Versement;

import ma.octo.assignement.repository.VersementRepository;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;

import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VersementServiceImplTest {
    @Mock
    private VersementRepository versementRepository;


    @InjectMocks
    private VersementServiceImpl versementService;

    @Test
    void getAllVersement() {
        Versement versement1 = new Versement();
        versement1.setCompteBeneficiaire(new Compte());
        versement1.setNomEmetteur("MACHROU");
        versement1.setMotifVersement("Assignement 2021");
        versement1.setMontantVersement(new BigDecimal(500));

        Versement versement2 = new Versement();
        versement2.setCompteBeneficiaire(new Compte());
        versement2.setNomEmetteur("TOUIJER");
        versement2.setMotifVersement("Assignement 2021");
        versement2.setMontantVersement(new BigDecimal(130));

        List<Versement> versementList = Arrays.asList(versement1, versement2);

        when(versementRepository.findAll()).thenReturn(versementList);
        List<VersementDto> versementDtoList = versementService.getAllVersement();

        Assertions.assertEquals("Assignement 2021", versementDtoList.get(0).getMotifVersement());
        Assertions.assertEquals("MACHROU", versementDtoList.get(0).getNomEmetteur());
        Assertions.assertEquals(new BigDecimal(500), versementDtoList.get(0).getMontantVersement());


        Assertions.assertEquals("TOUIJER", versementDtoList.get(1).getNomEmetteur());
        Assertions.assertEquals(new BigDecimal(130), versementDtoList.get(1).getMontantVersement());
    }


    @Test
    void saveVersement() {
    }
}