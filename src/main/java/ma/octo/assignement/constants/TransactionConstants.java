package ma.octo.assignement.constants;

public class TransactionConstants {
    public static final int MONTANT_MAXIMAL_VIREMENT = 10000;
    public static final int MONTANT_MINIMAL_VIREMENT = 10;
    public static final int MONTANT_MAXIMAL_VERSEMENT = 8000;
    public static final int MONTANT_MINIMAL_VERSEMENT = 100;
}
