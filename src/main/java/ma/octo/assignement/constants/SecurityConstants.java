package ma.octo.assignement.constants;

public class SecurityConstants {

    public final static String SECRET = "wafae@ma.gmail";
    public final static String TOKEN_PREFIX="Bearer ";
    public final static String HEADER_STRING="Authorization";
    public final static long EXPIRATION_TIME = 846000000;//10days
}

