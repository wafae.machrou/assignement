package ma.octo.assignement.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.enumeration.TransactionType;

import javax.persistence.*;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "AUDIT_TRANSACTION")
public class AuditTransaction {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;

        @Column(length = 100)
        private String message;

        @Enumerated(EnumType.STRING)
        private TransactionType transactionType;

}
