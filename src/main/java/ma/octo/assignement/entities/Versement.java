package ma.octo.assignement.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
@Table(name = "VERSEMENT")
public class Versement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateVersement;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantVersement;
  @Column(length = 200)
  private String motifVersement;
  @Column(length = 60, nullable = false)
  private String nomEmetteur;
  @Column(length = 60, nullable = false)
  private String prenomEmetteur;

  private String cinEmetteur;

  @ManyToOne
  private Compte compteBeneficiaire;


    public Versement(long l, BigDecimal bigDecimal, Date date, String nom, Compte compte, String s) {
    }
}
