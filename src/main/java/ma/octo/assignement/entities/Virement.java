package ma.octo.assignement.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
@Table(name = "VIREMENT")
public class Virement{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateVirement;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantVirement;
  @Column(length = 200)
  private String motifVirement;
  @ManyToOne
  private Compte compteEmetteur;
  @ManyToOne
  private Compte compteBeneficiaire;

}
