package ma.octo.assignement.repository;

import ma.octo.assignement.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<AppRole, Long> {
  AppRole findByRoleName(String roleName);
}
