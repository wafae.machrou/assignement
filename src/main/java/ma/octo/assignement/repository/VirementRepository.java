package ma.octo.assignement.repository;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Virement;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Date;
import java.util.List;


public interface VirementRepository extends JpaRepository<Virement, Long> {

    List<Virement> findByCompteEmetteur(Compte compte);

    List<Virement> findByCompteEmetteurAndDateVirementBetween(Compte compte,Date dateDebut,Date dateFin);

}
