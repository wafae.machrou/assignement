package ma.octo.assignement.repository;

import ma.octo.assignement.entities.Compte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte, Long> {
  Compte findByNumeroCompte(String numeroCompte);
  Compte findByRib(String rib);


}
