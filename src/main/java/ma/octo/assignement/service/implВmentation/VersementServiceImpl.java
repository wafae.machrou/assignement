package ma.octo.assignement.service.implВmentation;


import ma.octo.assignement.dto.VersementDto;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Versement;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.enumeration.TransactionType;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;


import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IAuditTransactionService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import static ma.octo.assignement.constants.TransactionConstants.*;
@Service
@Transactional
public class VersementServiceImpl implements IVersementService {


    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImpl.class);


    @Autowired
    private VersementRepository versementRepository;

    @Autowired
    private ICompteService compteService;

    @Autowired
    private IAuditTransactionService auditTransactionService;

    @Override
    public List<VersementDto> getAllVersement() {
        List<Versement> allVersements = versementRepository.findAll();
        return VersementMapper.toListDto(allVersements);

    }

    @Override
    public VersementDto saveVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException
        {


            Compte compteBenificiare = compteService.findByRib(versementDto.getRib());

            verifyCompte(compteBenificiare);
            verifyMontant(versementDto.getMontantVersement());

            compteBenificiare.setSolde(compteBenificiare.getSolde().add(versementDto.getMontantVersement()));
            compteService.saveCompte(compteBenificiare);

            Versement versement = VersementMapper.versementdtoToVersement(versementDto);
            versement.setCompteBeneficiaire(compteBenificiare);
            versement.setDateVersement(new Date());
            versementRepository.save(versement);

            auditTransactionService.auditTransaction("Versement de " + versementDto.getNomEmetteur()+" "+versementDto.getPrenomEmetteur()+ " vers le compte ayant le rib : " + versementDto
                    .getRib() + " d'un montant de " + versementDto.getMontantVersement()
                    .toString(), TransactionType.VERSEMENT);

            return VersementMapper.versementToVersementDto(versement);
        }



    private void verifyCompte(Compte compteBenificiare) throws CompteNonExistantException {
        if (compteBenificiare == null) {
            LOGGER.error("Compte bénéficiare non existant");
            throw new CompteNonExistantException("Compte bénéficiare non existant");
        }
    }
    private void verifyMontant(BigDecimal montant) throws TransactionException {
        if (montant.intValue() < MONTANT_MINIMAL_VERSEMENT) {
            LOGGER.error("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (montant.intValue() >MONTANT_MAXIMAL_VERSEMENT) {
            LOGGER.error("Montant maximal de versement dépassé");
            throw new TransactionException("Montant maximal de versement dépassé");
        }
    }


}
