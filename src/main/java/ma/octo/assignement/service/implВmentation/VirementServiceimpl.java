package ma.octo.assignement.service.implВmentation;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.enumeration.TransactionType;
import ma.octo.assignement.exceptions.CompteNonExistantException;

import ma.octo.assignement.exceptions.NombreVirementsPermisAtteintException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;


import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IAuditTransactionService;
import ma.octo.assignement.service.IVirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static ma.octo.assignement.constants.TransactionConstants.*;

@Service
@Transactional
public class VirementServiceimpl implements IVirementService {

    Logger LOGGER = LoggerFactory.getLogger(VirementServiceimpl.class);

    private final CompteServiceImpl compteService;

    private final VirementRepository virementRepository;

    private final IAuditTransactionService auditTransactionService;

    private final UtilisateurRepository utilisateurRepository;

    public VirementServiceimpl(CompteServiceImpl compteService, VirementRepository virementRepository, IAuditTransactionService auditTransactionService, UtilisateurRepository utilisateurRepository) {
        this.compteService = compteService;
        this.virementRepository = virementRepository;
        this.auditTransactionService = auditTransactionService;
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public List<VirementDto> getAllVirements() {
        List<Virement> allVirements = virementRepository.findAll();
        return VirementMapper.toListDto(allVirements);
    }

    @Override
    public VirementDto saveVirement(VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException, NombreVirementsPermisAtteintException {

            Compte compteEmetteur = compteService.findByNumeroCompte(virementDto.getNrCompteEmetteur());
            Compte compteBenificiare = compteService.findByNumeroCompte(virementDto.getNrCompteBeneficiaire());

            verifyCompte(compteEmetteur,compteBenificiare);

            verifyNombreDeVirement(compteEmetteur);

            verifyMontant(virementDto.getMontantVirement());

            verifyMotif(virementDto.getMotif());

            verifySolde(compteEmetteur.getSolde(),virementDto.getMontantVirement());


            compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
            compteService.saveCompte(compteEmetteur);

            compteBenificiare
                    .setSolde(compteBenificiare.getSolde().add(virementDto.getMontantVirement()));
            compteService.saveCompte(compteBenificiare);

            Virement virement = VirementMapper.virementdtoToVirement(virementDto);
            virement.setCompteEmetteur(compteEmetteur);
            virement.setCompteBeneficiaire(compteBenificiare);
            virement.setDateVirement(new Date());
            virementRepository.save(virement);

            auditTransactionService.auditTransaction("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                    .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                    .toString(), TransactionType.VIREMENT);

            return VirementMapper.virementToVirementDto(virement);
        }

      private void verifyCompte(Compte compteEmetteur , Compte compteBenificiare) throws CompteNonExistantException {
          if (compteEmetteur == null) {
              LOGGER.error("Compte emetteur non existant");
              throw new CompteNonExistantException("Compte emetteur non existant");
          }
          if (compteBenificiare == null) {
              LOGGER.error("Compte bénéficiare non existant");
              throw new CompteNonExistantException("Compte bénéficiare non existant");
          }
      }
      private  void verifyMontant(BigDecimal montant) throws TransactionException {

          if (montant.intValue() < MONTANT_MINIMAL_VIREMENT) {
              LOGGER.error("Montant minimal de virement non atteint");
              throw new TransactionException("Montant minimal de virement non atteint");
          } else if (montant.intValue() > MONTANT_MAXIMAL_VIREMENT) {
              LOGGER.error("Montant maximal de virement dépassé");
              throw new TransactionException("Montant maximal de virement dépassé");
          }
      }
      private void verifyMotif(String motif) throws TransactionException {
          if (motif.length() <= 0) {
              LOGGER.error("Motif vide");
              throw new TransactionException("Motif vide");
          }
      }
      private void verifySolde(BigDecimal soldeActuel,BigDecimal montant) throws SoldeDisponibleInsuffisantException {
          if (soldeActuel.intValue() < montant.intValue()) {
              LOGGER.error("Solde insuffisant pour l'emmeteur");
              throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'emmeteur");
          }

      }
      private void verifyNombreDeVirement(Compte compteEmetteur) throws NombreVirementsPermisAtteintException {
        Date dateDebut=new Date();
        dateDebut.setHours(00);
        dateDebut.setSeconds(00);
        dateDebut.setTime(00);
          Date dateFin=new Date();
          dateFin.setHours(23);
          dateFin.setSeconds(59);
          dateFin.setTime(00);
        System.out.println(dateFin);
       List<Virement> virements=
                virementRepository.findByCompteEmetteurAndDateVirementBetween(compteEmetteur,dateDebut,dateFin);
       if(virements.size()>10){
           LOGGER.error("Nombre Maximale de virements par jour atteint");
           throw new NombreVirementsPermisAtteintException("Nombre Maximale de virements par jour atteint");
       }
      }

}