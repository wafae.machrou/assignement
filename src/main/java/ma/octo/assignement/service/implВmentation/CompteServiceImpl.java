package ma.octo.assignement.service.implВmentation;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class CompteServiceImpl implements ICompteService {

    @Autowired
    private  CompteRepository compteRepository;

    @Override
    public Compte saveCompte(Compte compte) {
        return compteRepository.save(compte);
    }

    @Override
    public List<CompteDto> getAllCompte() {
        return CompteMapper.tocompteDtoList(compteRepository.findAll());
    }

    @Override
    public Compte findByNumeroCompte(String numeroCompte) {
        return compteRepository.findByNumeroCompte(numeroCompte);
    }

    @Override
    public Compte findByRib(String rib) {
        return compteRepository.findByRib(rib);
    }
}
