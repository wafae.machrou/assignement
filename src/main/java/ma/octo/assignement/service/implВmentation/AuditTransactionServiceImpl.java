package ma.octo.assignement.service.implВmentation;

import ma.octo.assignement.entities.AuditTransaction;
import ma.octo.assignement.enumeration.TransactionType;
import ma.octo.assignement.repository.AuditTransactionRepository;
import ma.octo.assignement.service.IAuditTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;

@Service
@Transactional
public class AuditTransactionServiceImpl implements IAuditTransactionService {

    Logger LOGGER = LoggerFactory.getLogger(AuditTransactionServiceImpl.class);

    @Autowired
    AuditTransactionRepository auditTransactionRepository;

    @Override
    public void auditTransaction(String message, TransactionType typeTransaction) {
        LOGGER.info("Audit de l'événement {}", typeTransaction);

        AuditTransaction audit = new AuditTransaction();
        if (typeTransaction.equals(TransactionType.VIREMENT))
            audit.setTransactionType(TransactionType.VIREMENT);
        else {
            audit.setTransactionType(TransactionType.VERSEMENT);
        }
        audit.setMessage(message);
        auditTransactionRepository.save(audit);
    }
}
