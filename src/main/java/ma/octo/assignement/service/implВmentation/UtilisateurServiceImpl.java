package ma.octo.assignement.service.implВmentation;

import ma.octo.assignement.entities.AppRole;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.repository.RoleRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private RoleRepository roleRepository;
    @Override
    public List<Utilisateur> getAllUtilisateur() {
        return utilisateurRepository.findAll();
    }

    @Override
    public Utilisateur findByUsername(String username) {
        return utilisateurRepository.findByUsername(username);
    }

    @Override
    public Utilisateur saveUser(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        Utilisateur user=utilisateurRepository.findByUsername(username);
        AppRole role=roleRepository.findByRoleName(roleName);
        user.getRoles().add(role);
    }
}
