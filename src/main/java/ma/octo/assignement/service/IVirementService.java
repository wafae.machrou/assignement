package ma.octo.assignement.service;

import ma.octo.assignement.dto.VirementDto;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NombreVirementsPermisAtteintException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVirementService {



    List<VirementDto> getAllVirements();
    VirementDto saveVirement(VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException, NombreVirementsPermisAtteintException;


}
