package ma.octo.assignement.service;


import ma.octo.assignement.entities.Utilisateur;

import java.util.List;

public interface IUtilisateurService {

    List<Utilisateur> getAllUtilisateur();
     Utilisateur findByUsername(String username);
     Utilisateur saveUser(Utilisateur utilisateur);
     void addRoleToUser(String username,String roleName);

}
