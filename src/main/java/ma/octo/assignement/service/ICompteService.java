package ma.octo.assignement.service;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;

import java.util.List;

public interface ICompteService {

    Compte saveCompte(Compte compte);
    List<CompteDto>  getAllCompte();
    Compte findByNumeroCompte(String numeroCompte);
    Compte findByRib(String rib);
}
