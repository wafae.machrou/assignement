package ma.octo.assignement.service;

import ma.octo.assignement.enumeration.TransactionType;

public interface IAuditTransactionService {
    void auditTransaction(String message, TransactionType typeTransaction);
}
