package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVersementService {

        List<VersementDto> getAllVersement();
        VersementDto saveVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;

}
