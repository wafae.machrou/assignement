package ma.octo.assignement.exceptions;

public class SecurityException extends Exception {

  private static final long serialVersionUID = 1L;

  public SecurityException() {
  }

  public SecurityException(String message) {
    super(message);
  }
}
