package ma.octo.assignement.exceptions;

public class NombreVirementsPermisAtteintException extends Exception {

  private static final long serialVersionUID = 1L;

  public NombreVirementsPermisAtteintException() {
  }

  public NombreVirementsPermisAtteintException(String message) {
    super(message);
  }
}
