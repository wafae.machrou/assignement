package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data @NoArgsConstructor @AllArgsConstructor
public class VersementDto {

  private String nomEmetteur;
  private String prenomEmetteur;
  private String cinEmetteur;
  private String rib;
  private BigDecimal montantVersement;
  private Date date;
  private String motifVersement;
}
