package ma.octo.assignement.web;

import ma.octo.assignement.dto.CompteDto;

import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/comptes")
public class CompteController {

    private final ICompteService compteService;

    @Autowired
    public CompteController(ICompteService compteService) {
        this.compteService = compteService;
    }

    @GetMapping()
    public List<CompteDto> getAllCompt(){
        return compteService.getAllCompte();
    }
}
