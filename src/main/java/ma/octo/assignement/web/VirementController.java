package ma.octo.assignement.web;


import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.service.IVirementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/banque")
class VirementController {


    private final IVirementService virementService;
@Autowired

    VirementController(IVirementService virementServiceimpl) {
        this.virementService = virementServiceimpl;
    }

    @GetMapping("/virements")
    List<VirementDto> loadAll() {
        return virementService.getAllVirements();
    }


    @PostMapping("/virements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto) throws Exception {
        virementService.saveVirement(virementDto);
    }
}

