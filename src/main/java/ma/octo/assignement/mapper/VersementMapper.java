package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Versement;

import java.util.ArrayList;
import java.util.List;

public class VersementMapper {



        public static VersementDto versementToVersementDto(Versement versement) {

            VersementDto versementDto = new VersementDto();
            versementDto.setMontantVersement(versement.getMontantVersement());
            versementDto.setDate(versement.getDateVersement());
            versementDto.setRib(versement.getCompteBeneficiaire().getRib());
            versementDto.setCinEmetteur(versement.getCinEmetteur());
            versementDto.setNomEmetteur(versement.getNomEmetteur());
            versementDto.setPrenomEmetteur(versement.getPrenomEmetteur());
            versementDto.setMotifVersement(versement.getMotifVersement());
            return versementDto;

        }

        public static Versement versementdtoToVersement(VersementDto versementDto) {

            Versement versement = new Versement();
            Compte compteBeneficiare = new Compte();

            compteBeneficiare.setNumeroCompte(versementDto.getRib());
            versement.setPrenomEmetteur(versementDto.getPrenomEmetteur());
            versement.setNomEmetteur(versementDto.getNomEmetteur());
            versement.setCinEmetteur(versementDto.getNomEmetteur());
            versement.setMontantVersement(versementDto.getMontantVersement());
            versement.setDateVersement(versementDto.getDate());
            versement.setMotifVersement(versementDto.getMotifVersement());


            return versement;

        }
    public static List<VersementDto> toListDto(List<Versement> versements){
        if (versements.isEmpty()) {
            return null;
        }
        List<VersementDto> versementDtos=new ArrayList<>();
        versements.forEach(versement -> {
            versementDtos.add(versementToVersementDto(versement));
        });

        return versementDtos;
    }


}
