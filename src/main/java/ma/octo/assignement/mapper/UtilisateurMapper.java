package ma.octo.assignement.mapper;


import ma.octo.assignement.dto.AuthDto;

import ma.octo.assignement.entities.Utilisateur;



public class UtilisateurMapper {


    public static AuthDto ToAuthDto(Utilisateur utilisateur) {
        AuthDto authDto=new AuthDto();
        authDto.setUsername(utilisateur.getUsername());
        authDto.setPassword(utilisateur.getPassword());

        return authDto;

    }



}
