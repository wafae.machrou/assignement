package ma.octo.assignement.mapper;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.dto.VirementDto;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VirementMapper {


    public static VirementDto virementToVirementDto(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(Objects.requireNonNullElse(virement.getCompteEmetteur(), new Compte()).getNumeroCompte());
        virementDto.setNrCompteBeneficiaire(Objects.requireNonNullElse(virement.getCompteBeneficiaire(), new Compte()).getNumeroCompte());
        virementDto.setDate(virement.getDateVirement());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());
        return virementDto;

    }

    public static Virement virementdtoToVirement(VirementDto virementDto) {
        Virement virement = new Virement();
        Compte compteEmetteur = new Compte();
        Compte compteRecepteur = new Compte();
        compteEmetteur.setNumeroCompte(virementDto.getNrCompteEmetteur());
        compteRecepteur.setNumeroCompte(virementDto.getNrCompteBeneficiaire());
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());
        virement.setCompteBeneficiaire(compteRecepteur);
        return virement;

    }

    public static List<VirementDto> toListDto(List<Virement> virements){
        if (virements.isEmpty()) {
            return null;
        }
        List<VirementDto> virementDtos=new ArrayList<>();
        virements.forEach(virement -> {
            virementDtos.add(virementToVirementDto(virement));
        });

        return virementDtos;
    }



}
