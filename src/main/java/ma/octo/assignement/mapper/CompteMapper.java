package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.entities.Compte;


import java.util.ArrayList;
import java.util.List;


public class CompteMapper {



    public static CompteDto toCompteDto(Compte compte) {
        CompteDto compteDto=new CompteDto();
        compteDto.setNumeroCompte(compte.getNumeroCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());
        compteDto.setNomUtilisateur(compte.getUtilisateur().getLastname());
        compteDto.setPrenomUtilisateur(compte.getUtilisateur().getFirstname());

        return compteDto;

    }
    public  static List<CompteDto> tocompteDtoList(List<Compte> comptes){
        if (comptes.isEmpty()) {
            return null;
        }
        List<CompteDto> compteDtos=new ArrayList<>();
        comptes.forEach(virement -> {
            compteDtos.add(toCompteDto(virement));
        });

        return compteDtos;
    }
}
