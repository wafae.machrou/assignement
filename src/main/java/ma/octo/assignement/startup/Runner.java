package ma.octo.assignement.startup;

import java.math.BigDecimal;
import java.util.Date;

import ma.octo.assignement.entities.AppRole;
import ma.octo.assignement.enumeration.Sexe;

import ma.octo.assignement.repository.RoleRepository;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import ma.octo.assignement.entities.Compte;
import ma.octo.assignement.entities.Utilisateur;
import ma.octo.assignement.entities.Virement;
import ma.octo.assignement.repository.CompteRepository;

import ma.octo.assignement.repository.VirementRepository;

import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    @Autowired
    private ICompteService compteService;
    @Autowired
    private IUtilisateurService utilisateurService;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private RoleRepository roleRepository;


    @Override
    public void run(String... args) throws Exception {

        AppRole appRole=new AppRole();
        appRole.setRoleName("ADMIN");
        roleRepository.save(appRole);

        AppRole appRole2=new AppRole();
        appRole2.setRoleName("USER");
        roleRepository.save(appRole2);

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setSexe(Sexe.FEMME);

        utilisateurService.saveUser(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setSexe(Sexe.HOMME);

        utilisateurService.saveUser(utilisateur2);

        utilisateurService.addRoleToUser(utilisateur1.getUsername(),"ADMIN");
        utilisateurService.addRoleToUser(utilisateur2.getUsername(),"USER");

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteService.saveCompte(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        compteService.saveCompte(compte2);

        Virement v = new Virement();
        v.setMontantVirement(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateVirement(new Date());
        v.setMotifVirement("Assignment 2021");

        virementRepository.save(v);
    }

}